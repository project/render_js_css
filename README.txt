Introduction:
-------------------
This module provide functionality to remove (unset) css or js files
for selected content type.You have to enter path of the css or js file
and selected js and css files will be remove (unset) from pages
which are related to selected content type. 

Requirements:
------------------
This module requires the following modules:
* None

Installation:
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

How To Use:
-----------------
1. Go to Configuration -> User interface -> Render JS and CSS.
2. Select content type.
3. Enter Path of css or js file.
4. Click Save

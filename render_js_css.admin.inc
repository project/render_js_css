<?php

/**
 * @file
 * Admin Configuration file for render_js_css module.
 */

/**
 * Setting form.
 */
function render_js_css_admin_settings_form($form, $form_state) {
  $edit = 0;
  $qry = drupal_get_query_parameters();
  if (count($qry) > 0) {
    $edit = $qry['edit'];
    $get_edit_value = render_js_css_get_defaul_value('remove_file_url', $edit);
    if (empty($get_edit_value)) {
      $edit = 0;
    }
  }
  $form = array();
  $form['cssfieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'CSS Configuration',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['cssfieldset']['content_type'] = array(
    '#type' => 'select',
    '#title' => t("Content Type"),
    '#required' => TRUE,
    '#options' => render_js_css_get_content_type_list($edit),
    '#description' => t('Please select content type.'),
    '#default_value' => array(render_js_css_get_defaul_value('content_type', $edit)),
  );

  $form['cssfieldset']['remove_file_url'] = array(
    '#title' => t('Add path to remove files'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#description' => t('Add files path Ex "sites/all/modules/ctools/css/ctools.css" or "sites/all/modules/ctools/css/ctools.js".  Enter one path per line.'),
    '#default_value' => render_js_css_get_defaul_value('remove_file_url', $edit),
  );

  $form['cssfieldset']['id'] = array(
    '#type' => 'hidden',
    '#value' => $edit,
  );

  $form['cssfieldset']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (!empty($edit)) {
    $form['cssfieldset']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  $form['markup'] = array(
    '#markup' => render_js_css_rows(),
  );

  return $form;
}

/**
 * Validate form.
 */
function render_js_css_admin_settings_form_validate($form, $form_state) {
  if ($form_state['triggering_element']["#parents"][0] !== 'delete') {
    $remove_file_url = $form_state['values']['remove_file_url'];
    $file_array = explode(PHP_EOL, $remove_file_url);
    $file_array = array_map('trim', $file_array);
    if (count($file_array) > 0 && !empty(trim($remove_file_url))) {
      foreach ($file_array as $value) {
        $check = render_js_css_check_extention($value);
        if ($check) {
          form_set_error('remove_file_url', t("Please enter valide Paths."));
        }
      }
    }
  }
}

/**
 * Perform submit, edit and delete action.
 */
function render_js_css_admin_settings_form_submit($form, $form_state) {
  if ($form_state['triggering_element']["#parents"][0] === 'save') {
    $content_type = $form_state['values']['content_type'];
    $remove_file_url = $form_state['values']['remove_file_url'];
    $edit = $form_state['values']['id'];
    if (!empty($edit)) {
      db_update('render_js_css')
        ->fields(array(
          'content_type' => $content_type,
          'remove_file_url' => $remove_file_url,
        ))
        ->condition('content_type', $edit)
        ->execute();
      drupal_set_message(t("Record has been updated."));
      drupal_goto('admin/config/user-interface/render-js-css');
    }
    else {
      db_insert('render_js_css')
        ->fields(array(
          'content_type' => $content_type,
          'remove_file_url' => $remove_file_url,
        ))
        ->execute();
      drupal_set_message(t("Record has been saved."));
    }
  }
  if ($form_state['triggering_element']["#parents"][0] === 'delete') {
    $edit = $form_state['values']['id'];
    if (!empty($edit)) {
      db_delete('render_js_css')
        ->condition('content_type', $edit, '=')
        ->execute();
      drupal_set_message(t("Record has been deleted."));
      drupal_goto('admin/config/user-interface/render-js-css');
    }
  }
}

/**
 * Get content type saved list.
 */
function render_js_css_get_content_type_list($enable_edit) {
  $content_type = array("" => "-Select Content Type-");
  if (!empty($enable_edit)) {
    $content_type_name['field_name'] = 'name';
    $content_type_name['field_id'] = $enable_edit;
    $select_content_type_value = render_js_css_get_all_content_type($content_type_name);
    if ($enable_edit === 'home') {
      $select_content_type_value = 'Home Page';
    }
    $content_type[$enable_edit] = $select_content_type_value;
  }
  $saved_content_type = render_js_css_saved_content_type();
  if (!in_array('home', $saved_content_type)) {
    $content_type['home'] = t("Home page");
  }
  $result_content_type = render_js_css_get_all_content_type();
  foreach ($result_content_type as $value) {
    if (!in_array($value->type, $saved_content_type)) {
      $content_type[$value->type] = $value->name;
    }
  }
  return $content_type;
}

/**
 * Get list of saved content type.
 */
function render_js_css_saved_content_type() {
  $saved_content_type = array();
  $results = db_select('render_js_css', 'rjc')
    ->fields('rjc', array('content_type'))
    ->execute();
  foreach ($results as $value) {
    $saved_content_type[] = $value->content_type;
  }
  return $saved_content_type;
}

/**
 * Get list of all records.
 */
function render_js_css_rows() {

  $html = '';
  $rows = array();
  $header = array(
    t('Sn.'),
    t('Content Type'),
    t('Remove Files'),
    t('Opration'),
  );

  $query = db_select('render_js_css', 'tcj');
  $query->fields('tcj', array('id', 'content_type', 'remove_file_url'));
  $query = $query->extend('PagerDefault')->limit(10);
  $query->orderBy('tcj.timestamp', 'DESC');
  $result = $query->execute();

  $sn = 0;
  while ($record = $result->fetchObject()) {
    $content_type_name['field_name'] = 'name';
    $content_type_name['field_id'] = $record->content_type;
    if ($record->content_type == 'home') {
      $content_name = 'Home Page';
    }
    else {
      $content_name = render_js_css_get_all_content_type($content_type_name);
    }
    $rows[] = array(
      ++$sn,
      $content_name,
      check_markup($record->remove_file_url),
      l(t('Edit'), 'admin/config/user-interface/render-js-css', array('query' => array('edit' => $record->content_type))),
    );
  }

  $html .= theme('table', array('header' => $header, 'rows' => $rows));
  $html .= theme('pager');

  return $html;
}

/**
 * Check path extension.
 */
function render_js_css_check_extention($file_url) {
  if (!empty($file_url)) {
    $path_info = pathinfo($file_url);
    if (trim($path_info['extension']) === 'css' || trim($path_info['extension']) === 'js') {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}

/**
 * Get get content type list.
 */
function render_js_css_get_all_content_type($extra = array()) {
  $query = db_select('node_type', 'nt');
  if (count($extra) > 0) {
    $query->fields('nt', array($extra['field_name']));
    $query->condition('type', $extra['field_id']);
    $results = $query->execute()->fetchField();
  }
  else {
    $query->fields('nt', array('type', 'name'));
    $results = $query->execute();
  }
  return $results;
}
